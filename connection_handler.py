from socketserver import StreamRequestHandler, TCPServer, ThreadingMixIn

# Helper function to use custom function or class method as a request handler
# thus allowing handlers of different requests to share state.
def create_server(address, handler):
    class ThreadedTCPRequestHandler(StreamRequestHandler):
        def handle(self):
            handler(self)

    class ThreadedTCPServer(ThreadingMixIn, TCPServer):
        pass

    return ThreadedTCPServer(address, ThreadedTCPRequestHandler)
