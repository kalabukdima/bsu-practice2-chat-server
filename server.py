import sys
import logging
import argparse
import json

import connection_handler


def main():
    logging.getLogger().setLevel(logging.INFO)
    args = parse_args()
    server = Server(args.port)
    server.serve()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("port", type=int)
    return parser.parse_args()


def format_address(address):
    ip, port = address
    return f"{ip}:{port}"


class Peer:
    def __init__(self, request_handler: connection_handler.StreamRequestHandler):
        self.address = request_handler.client_address
        self.wfile = request_handler.wfile
        self.username = None


class Server():
    def __init__(self, port):
        self.request_server = connection_handler.create_server(
            ("localhost", port),
            lambda handler: self.handle_connection(handler)
        )
        self.peers = []

    def serve(self):
        logging.info("Listening for new connections")
        self.request_server.serve_forever()

    def handle_connection(self, request_handler):
        peer = Peer(request_handler)
        self.connect(peer)

        for line in request_handler.rfile:
            event = json.loads(line)
            self.process_event(event, sender=peer)

        self.disconnect(peer)

    def connect(self, peer):
        logging.info("%s connected", format_address(peer.address))
        self.peers.append(peer)

    def disconnect(self, peer):
        self.broadcast(peer, f"{peer.username} has left chat\n")
        logging.info("%s disconnected", format_address(peer.address))
        self.peers.remove(peer)

    def process_event(self, event, sender):
        if event["type"] == "set_username":
            self.set_username(sender, event["username"])
        elif event["type"] == "message":
            self.send_message(sender, event["body"])
        else:
            raise ValueError("Unknown event type: " + event["type"])

    def set_username(self, peer, username):
        if peer.username is None:
            self.broadcast(peer, f"{username} has joined chat\n")
        else:
            self.broadcast(peer, f"{peer.username} is now {username}\n")
        peer.username = username

    def send_message(self, sender, message):
        if sender.username is None:
            logging.warning("Anonymous messages are not allowed")
            return
        self.broadcast(sender, f"{sender.username}> {message}\n")

    def broadcast(self, sender, text):
        for peer in self.peers:
            if peer is not sender:
                peer.wfile.write(text.encode("UTF-8"))
        sys.stdout.write(text)


if __name__ == '__main__':
    main()
