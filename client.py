import sys
import logging
import argparse

import re
import socket
import select
import json


def main():
    logging.getLogger().setLevel(logging.INFO)
    args = parse_args()
    client = ChatClient(args.ip, args.port, args.username)
    client.run()


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("ip")
    parser.add_argument("port", type=int)
    parser.add_argument("--username")
    return parser.parse_args()


# Returns a subset of streams which are ready to be read from
def wait_for_input(streams):
    readable, _, _ = select.select(streams, [], [])
    return readable


class ChatClient:
    def __init__(self, ip, port, username):
        self.sock = socket.create_connection((ip, port))
        self.sock_file = self.sock.makefile()

        self.username = username

    def run(self):
        if self.username is not None:
            self.send_username(self.username)
        self.start_message_loop()

    def start_message_loop(self):
        input_handlers = {
            self.sock_file: self.handle_network_message,
            sys.stdin: self.handle_input
        }
        both_ends_open = True
        while both_ends_open:
            for stream in wait_for_input(list(input_handlers.keys())):
                line = stream.readline()
                if len(line) == 0:
                    both_ends_open = False
                    break
                input_handlers[stream](line)

        self.sock.close()
        logging.info("Connection closed")

    def handle_network_message(self, line):
        sys.stdout.write(line)
        sys.stdout.flush()

    def handle_input(self, line):
        commands = [
            ("/username (?P<username>\\w+)$", self.send_username),
            ("(?P<message>.*)$", self.send_message),
        ]
        for pattern, handler in commands:
            m = re.match(pattern, line)
            if m is not None:
                handler(**m.groupdict())
                break
        else:
            logging.warning("Could not parse input")

    def send_username(self, username):
        self.send_event({
            "type": "set_username",
            "username": username,
        })

    def send_message(self, message):
        self.send_event({
            "type": "message",
            "body": message,
        })

    def send_event(self, event):
        string = json.dumps(event)
        self.sock.sendall(string.encode("UTF-8") + b"\n")


if __name__ == '__main__':
    main()
